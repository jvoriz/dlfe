#!/bin/bash

set -e

#Prevent running alone
if [ "${SCRIPT_DIR}" = "" ]; then
	echo "script ${0} could not be executed standalone outside context, exiting..."
	exit 1
fi

#functions
pxemenu_gen()
{
	info_msg "generating pxe menu example to $profile-$release-$arch.menu"
	cat > $build_dir/binary/live/$profile-$release-$arch.menu << EOF
LABEL ${profile} ${release} ${arch}
 KERNEL ${live_dir}/${profile}/${release}/${arch}/vmlinuz
 APPEND initrd=${live_dir}/${profile}/${release}/${arch}/initrd.img fetch=${pxe_fetchproto}://${tftp_host}/${pxe_topdir}${live_dir}/${profile}/${release}/${arch}/filesystem.squashfs ${append_defaults} ${append_opts}
EOF
}
pxemenu_append()
{
	if ! ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host grep -q "MENU\ BEGIN\ $profile" $tftp_root/$live_dir/$pxe_livemenu; then
		info_msg "appending pxe menu $profile to remote $pxe_livemenu"
		echo "MENU BEGIN $profile" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$pxe_livemenu"
		echo "  INCLUDE $pxe_livepath/$live_dir/$profile/$profile.menu" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$pxe_livemenu"
		echo "MENU END $profile" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$pxe_livemenu"
	fi
}
pxelabel_append()
{
	if ! ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host grep -q "LABEL\ $profile\ $release\ $arch" $tftp_root/$live_dir/$profile/$profile.menu; then
		info_msg "appending pxe label $profile $release $arch to remote $profile.menu"
		echo "LABEL $profile $release $arch" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$profile/$profile.menu"
		echo "  KERNEL ${pxe_livepath}/${live_dir}/${profile}/${release}/${arch}/vmlinuz" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$profile/$profile.menu"
		echo "  APPEND initrd=${pxe_livepath}/${live_dir}/${profile}/${release}/${arch}/initrd.img fetch=${pxe_fetchproto}://${tftp_host}/${pxe_topdir}${pxe_fetchlivepath}/${live_dir}/${profile}/${release}/${arch}/filesystem.squashfs ${append_defaults} ${append_opts}" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$profile/$profile.menu"
	fi
}
grubsubmenu_append()
{
	if ! ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host grep -q "$profile.grub" $tftp_root/$live_dir/$grub_livemenu; then
		info_msg "appending grub submenu $profile to remote $grub_livemenu"
		echo 'submenu "'$profile' >" {' | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$grub_livemenu"
		echo "  configfile ${pxe_topdir}$pxe_livepath/$live_dir/$profile/$profile.grub" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$grub_livemenu"
		echo "}" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$grub_livemenu"
	fi
}
grubentry_append()
{
	if ! ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host grep -q "$profile\ $release\ $arch" $tftp_root/$live_dir/$profile/$profile.grub; then
		info_msg "appending grub entry $profile $release $arch to remote $profile.grub"
		echo 'menuentry "'$profile $release $arch'" {' | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$profile/$profile.grub"
		echo "  linuxefi ${pxe_topdir}${pxe_livepath}/${live_dir}/${profile}/${release}/${arch}/vmlinuz fetch=${pxe_fetchproto}://${tftp_host}/${pxe_topdir}${pxe_fetchlivepath}/${live_dir}/${profile}/${release}/${arch}/filesystem.squashfs ${append_defaults} ${append_opts}" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$profile/$profile.grub"
		echo "  initrdefi ${pxe_topdir}${pxe_livepath}/${live_dir}/${profile}/${release}/${arch}/initrd.img" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$profile/$profile.grub"
		echo "}" | ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host -T "cat >>$tftp_root/$live_dir/$profile/$profile.grub"
	fi
}
