#!/bin/bash

set -e

#Prevent running alone
if [ "${SCRIPT_DIR}" = "" ]; then
	echo "script ${0} could not be executed standalone outside context, exiting..."
	exit 1
fi

#functions
scp_push()
{
	echo "pushing files to $tftp_host:$tftp_root/$live_dir/$profile/$release/$arch"
	for f in $1 ; do
		if [ -z $2 ]; then
			echo "pushing $f"
    	scp -P $tftp_port -i $tftp_pkey $build_dir/binary/live/$f $tftp_user@$tftp_host:$tftp_root/$live_dir/$profile/$release/$arch
		else
			echo "pushing $f to $2"
    	scp -P $tftp_port -i $tftp_pkey $build_dir/$f $tftp_user@$tftp_host:$tftp_root/$2
		fi
	done
}

rsync_push()
{
	echo "rsyncing whole content to $tftp_host:$tftp_root/$live_dir/$profile/$release/$arch"
	rsync -avz -e "ssh -p $tftp_port -i $tftp_pkey" $build_dir/binary/live/* $tftp_user@$tftp_host:$tftp_root/$live_dir/$profile/$release/$arch
}
