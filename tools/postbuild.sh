#!/bin/bash

set -e

#Prevent running alone
if [ "${SCRIPT_DIR}" = "" ]; then
	echo "script ${0} could not be executed standalone outside context, exiting..."
	exit 1
fi

#entering profile dir
pushd $profile_dir >/dev/null

###move some additionals chroot build logs to build logs dir
#for f in $build_dir/chroot/tmp/*.log ; do
#	mv $f $logs_dir
#done

###sourcing postbuild configurations / functions
file_sourcing_global_local $global_dir $profile_dir "postbuild.conf"
file_sourcing $tools_dir "postbuild-pxe.sh postbuild-tftp.sh"

##Auto push to tftp
if [ "$auto_push" = "1" ] && [ -f $tftp_pkey ]; then
	##check/create remote folder
	ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host mkdir -p $tftp_root/$live_dir/$profile/$release/$arch
	##arm missing files fix
	if [ "$arch" = "arm64" ]; then
		pushd $binaries_dir >/dev/null
		for live_file in $live_files; do
			if [ ! -f $live_file ]; then
        		latest_live_file=$(get_newest_file "${live_file}*")
        		ln -s $latest_live_file $live_file
        	fi
        done
        popd >/dev/null
	fi
	##Push
	#scp (will overwrite files)
	scp_push "$live_files"
	#OR
	#rsync (will copy whole binary/live content)
	#rsync_push
else
	echo "[auto_push disabled or private key not found, aborting]"
fi

##Generate pxe menu example
[ "$pxemenu_helper" = "1" ] && pxemenu_gen

##Integrate pxe menu to root menu
if [ "$pxemenu_integration" = "1" ]; then
	##check/create pxe live (main) menu file
	remote_file_touch $tftp_root/$live_dir/$pxe_livemenu
	##append this profile menu entry if not present in pxe live (main) menu
	pxemenu_append

	##check/create for pxe profile menu file
	remote_file_touch $tftp_root/$live_dir/$profile/$profile.menu
	##append this release/arch menu entry if not present in pxe profile menu file
	pxelabel_append
fi

##Integrate grub menu to root menu
if [ "$grubmenu_integration" = "1" ]; then
	##check/create grub live (main) menu file
	remote_file_touch $tftp_root/$live_dir/$grub_livemenu
	##append this profile menu entry if not present in grub live (main) menu
	grubsubmenu_append

	##check/create for pxe profile menu file
	remote_file_touch $tftp_root/$live_dir/$profile/$profile.grub
	##append this release/arch menu entry if not present in pxe profile menu file
	grubentry_append
fi

##Custom postbuild script
#start custom postbuild script if present
if [ ! -x $profile_dir/postbuild-custom.sh ]; then
	info_msg "[optional postbuild-custom script not found or not executable, ignoring...]"
else
	info_msg "[found optional postbuild-custom script, executing...]"
	$profile_dir/postbuild-custom.sh 2>&1 | tee $logs_dir/postbuild-custom.log
	if [ ! "${PIPESTATUS[0]}" -eq "0" ]; then
		warn_msg "[error executing optional postbuild-custom script]"
		info_msg "=> check logs in $logs_dir/postbuild-custom.log"
	else
		good_msg "[success executing optional postbuild-custom script]"
	fi
fi

#exiting profile dir
popd >/dev/null
