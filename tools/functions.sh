#!/bin/bash

#set -e

#Prevent running alone
if [ "${SCRIPT_DIR}" = "" ]; then
	echo "script ${0} could not be executed standalone outside context, exiting..."
	exit 1
fi

packages_check_upgrade()
{
	#$1=package(s) to install/upgrade
	for package in $1 ; do
		info_msg "[check/upgrade $package]"
		if [ $(dpkg-query -W -f='${Status}' "$package" 2>/dev/null | grep -c "ok installed") -eq 0 ] || [ $(apt list --upgradable 2>/dev/null | grep -c "$package") -gt 0 ]; then
			info_msg "[Installing/Upgrading $package]"
			DEBIAN_FRONTEND=noninteractive apt-get install -y $package
		fi
	done
}

foreign_arch_check()
{
	#$1=target architecture
	##Check/install/update additional package(s) needed for building target on foreign architecture
	#Install/update qemu-user-static/binfmt-support IF needed AND local architecture is not aarch64 AND target architecture is arm64
	if [ ! $(arch) = "aarch64" ] && [ $1 = "arm64" ]; then
		packages="qemu-user-static binfmt-support"
		packages_check_upgrade "$packages"
	fi
	#modprobe binfmt_misc
}

#file copy (overwrite and preserve rights)
file_copy()
{
	#$1=src_dir,$2=dst_dir,$3=file(s)
	#check/create dst dir
	[ -d $2 ] || mkdir -p $2
	for f in $3 ; do
    	if [ -f $1/$f ]; then
    		cp -fp $1/$f $2
    		info_msg "[$f copied to $2]"
    	else
    		warn_msg "[$1/$f not found, ignoring]"
    	fi
	done
}

#file append line if not present
file_append_line()
{
	#$1=dst_file,$2=line(s)
	#check/create dst dir
	[ ! -d "$(dirname $1)" ] && mkdir -p "$(dirname $1)"
	#check/create dst file
	[ -f $1 ] || touch $1
	for l in $2 ; do
    	if ! grep -q $l $1; then
    		echo $l >> $1
    		info_msg "[$l added to $1]"
    	else
    		info_msg "[$l already in $1, ignoring]"
    	fi
	done
}

#remote file touch if not exist
remote_file_touch()
{
	if ! ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host test -f $1; then
		info_msg "creating file $1"
		ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host touch $1
	fi
}

#remote mkdir if not exist
remote_mkdir()
{
	if ! ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host test -d $1; then
		info_msg "creating dir $1"
		ssh -p $tftp_port -i $tftp_pkey $tftp_user@$tftp_host mkdir -p $1
	fi
}

#dist file(s) deploying
dist_file_to_global()
{
	#$1=file_name(s)
	[ ! "$#" -eq 1 ] && fail_msg "[${FUNCNAME[0]}(): Bad args number, exiting]" quit
	#[ -z "$1" ] && fail_msg "[No file name(s) passed to ${FUNCNAME[0]}(), exiting]" quit
	for f in $1 ; do
		#add global default conf file if not present, from .dist file
		if [ ! -f $global_dir/$f ]; then
			cp -p $dist_dir/${f}.dist $global_dir/$f || fail_msg "[Copy failure from $dist_dir/${f}.dist to $global_dir/$f]" quit
			info_msg "[Copy success from $dist_dir/${f}.dist to $global_dir/$f]"
		else
			info_msg "[$global_dir/$f exist, skipping]"
		fi
	done
}
global_file_to_local()
{
	#$1=dst_path,$2=file_name(s)
	[ ! "$#" -eq 2 ] && fail_msg "[${FUNCNAME[0]}(): Bad args number, exiting]" quit
	#[ -z "$2" ] && fail_msg "[No file name(s) passed to ${FUNCNAME[0]}(), exiting]" quit
	for f in $2 ; do
		#copy conf file if not present, from global one
		if [ ! -f $1/$f ]; then
			[ ! -d $1 ] && fail_msg "[${FUNCNAME[0]}(): Directory $1 does not exist, exiting]" quit
			cp -p $global_dir/$f $1/$f || fail_msg "[Copy failure from $global_dir/${f} to $1/$f]" quit
			info_msg "[Copy success from $global_dir/${f} to $1/$f]"
		else
			info_msg "[$1/$f exist, skipping]"
		fi
	done
}

#file(s) sourcing
file_sourcing()
{
	#$1=path,$2=file_name(s)
	for f in $2 ; do
		#sourcing
		[ -f $1/$f ] && . $1/$f
	done
}

#file(s) sourcing, global and local (overrides)
file_sourcing_global_local()
{
	#$1=global_path,$2=local_path,$3=file_name(s)
	for f in $3 ; do
		#global sourcing
		[ -f $1/$f ] && . $1/$f
		#local sourcing (overrides)
		[ -f $2/$f ] && . $2/$f
	done
}

#get newest file from list
get_newest_file()
{
	#$1=files
	unset -v latest
	for f in $1; do
  		[[ $f -nt $latest ]] && latest=$f
	done
	echo "$latest"
}
