#!/bin/bash

set -e

#Prevent running alone
if [ "${SCRIPT_DIR}" = "" ]; then
	echo "script ${0} could not be executed standalone outside context, exiting..."
	exit 1
fi

#entering profile dir
pushd $profile_dir >/dev/null

#copy/overwrite all dirs and their contents from profile dir to build dir
for d in */ ; do
    [ -d "$d" ] && tar cf - "$d" | ( cd $build_dir/; tar xfp -)
done

#foreign arch check
foreign_arch_check $arch

###sourcing configuration
file_sourcing_global_local $global_dir $profile_dir "prebuild.conf"

###CHROOT PACKAGES
##add debian packages to install in chroot if needed
#[ ! -d config/package-lists ] && mkdir -p config/package-lists
file_append_line $build_dir/config/package-lists/custom.list.chroot "$chroot_packages"

###COMMON HOOKS/SCRIPTS
##CHROOT BUILD HOOKS
#add some live hooks from common dir to the build dir to be executed in chroot when building system
file_copy $common_dir/$hooks_dir/live $build_dir/$hooks_dir/live "$common_buildhooks"

##LIVE BOOT SCRIPTS
#add / override common boot config scripts used during the boot process of the generated live system
file_copy $common_dir/$chroot_includes_dir/lib/live/boot $build_dir/$chroot_includes_dir/lib/live/boot "$common_liveboots"

##LIVE CONFIG SCRIPTS
#add / override common live config scripts used during the start process of the generated live system
file_copy $common_dir/$chroot_includes_dir/lib/live/config $build_dir/$chroot_includes_dir/lib/live/config "$common_liveconfigs"

##Custom prebuild script
#start custom prebuild script if present in profile
if [ ! -x $profile_dir/prebuild-custom.sh ]; then
	info_msg "[optional prebuild-custom script not found or not executable, ignoring...]"
else
	info_msg "[found optional prebuild-custom script, executing...]"
	$profile_dir/prebuild-custom.sh 2>&1 | tee $logs_dir/prebuild-custom.log
	if [ ! "${PIPESTATUS[0]}" -eq "0" ]; then
		warn_msg "[error executing optional prebuild-custom script]"
		info_msg "=> check logs in $logs_dir/prebuild-custom.log"
	else
		good_msg "[success executing optional prebuild-custom script]"
	fi
fi

#exiting profile dir
popd >/dev/null
