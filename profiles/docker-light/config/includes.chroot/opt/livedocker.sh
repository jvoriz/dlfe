#!/bin/bash

HOSTNAME=$(hostname)

## Default
ExecStartCmd="/usr/bin/dockerd --containerd=/run/containerd/containerd.sock"
DockRootPath="--data-root=/tmp/docker"

## Parse kernel command line
getsettings ()
{
  for _PARAMETER in $(cat /proc/cmdline)
    do
      case "${_PARAMETER}" in
        live-config.dockexpose=*|dockexpose=*)
          if [ "${_PARAMETER#*dockexpose=}" = "1" ]
          then
            ExecStartCmd="${ExecStartCmd} -H=tcp://0.0.0.0:2375 -H=unix:///var/run/docker.sock --tls=false"
          fi
        ;;
        live-config.dockroot=*|dockroot=*)
          if [ "${_PARAMETER#*dockroot=}" = "auto" ]
          then
            if mountpoint -q /mnt/$HOSTNAME
            then
              DockRootPath="--data-root=/mnt/$HOSTNAME/docker"
            fi
          else
            DockRootPath="--data-root=${_PARAMETER#*dockroot=}"
          fi
        ;;
        live-config.insecreg=*|insecreg=*)
          ExecStartCmd="${ExecStartCmd} --insecure-registry=${_PARAMETER#*insecreg=}"
        ;;
      esac
    done
}

## Applying settings
applysettings ()
{
  [ ! -d /etc/systemd/system/docker.service.d ] && mkdir /etc/systemd/system/docker.service.d
  echo '[Service]' >/etc/systemd/system/docker.service.d/override.conf
  echo 'ExecStart=' >>/etc/systemd/system/docker.service.d/override.conf
  echo "ExecStart=${ExecStartCmd} ${DockRootPath}" >>/etc/systemd/system/docker.service.d/override.conf
  systemctl daemon-reload && systemctl restart docker
}

case "${1:-}" in
  start)
    echo starting script $0
      getsettings
      applysettings
  ;;
  stop)
    echo stopping script $0
  ;;
  status)
    echo status of script $0
  ;;
  *)
    echo "Usage: ${0:-} {start|stop|status}" >&2
    exit 1
  ;;
esac
