#!/bin/bash

#portainer
portainer-start ()
{
  # Reading kernel command line
  for _PARAMETER in $(cat /proc/cmdline)
    do
      case "${_PARAMETER}" in
        live-config.portainer=*|portainer=*)
          if [ "${_PARAMETER#*portainer=}" = "1" ]
          then
            if [ -d /opt/portainer ]
            then
              tar -cC '/opt/portainer' . | docker load
              docker run -dti -p 9000:9000 --name portainer --hostname portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock --pull never portainer/portainer-ce -H unix:///var/run/docker.sock
            fi
          fi
        ;;
      esac
    done
}

case "${1:-}" in
  start)
    echo starting script $0
      portainer-start
  ;;
  stop)
    echo stopping script $0
  ;;
  status)
    echo status of script $0
  ;;
  *)
    echo "Usage: ${0:-} {start|stop|status}" >&2
    exit 1
  ;;
esac
