#!/bin/bash

#chromium
chromium-start ()
{
  # Empty options
  chromium_args=""
  # Reading kernel command line
  for _PARAMETER in $(cat /proc/cmdline)
    do
      case "${_PARAMETER}" in
        live-config.kiosk=*|kiosk=*)
          if [ "${_PARAMETER#*kiosk=}" = "1" ]
          then
          	#set kiosk mode
		  	chromium_args+=" --kiosk"
		  fi
        ;;
        live-config.startpage=*|startpage=*)
          #set startpage
          startpage="${_PARAMETER#*startpage=}"
		  chromium_args+=" $startpage"
        ;;
      esac
    done
  # Append concatenated chromium options
  sed -i -e 's@CHROMIUM_OPTS@'"${chromium_args}"'@' /etc/xdg/openbox/autostart
}

case "${1:-}" in
  start)
    echo starting script $0
      chromium-start
  ;;
  stop)
    echo stopping script $0
  ;;
  status)
    echo status of script $0
  ;;
  *)
    echo "Usage: ${0:-} {start|stop|status}" >&2
    exit 1
  ;;
esac
