#!/bin/sh

## Proxmox install script

set -e

echo [starting $0]

###functions
##disable enterprise depot
disable_repo()
{
	[ -f /etc/apt/sources.list.d/pve-enterprise.list ] && sed -i '1 s/^/#/g' /etc/apt/sources.list.d/pve-enterprise.list || true
}
##remove boot stuff hooks
boot_clean()
{
	[ -f /etc/kernel/postinst.d/zz-proxmox-boot ] && rm /etc/kernel/postinst.d/zz-proxmox-boot || true
	[ -f /etc/kernel/postrm.d/zz-proxmox-boot ] && rm /etc/kernel/postrm.d/zz-proxmox-boot || true
	[ -f /etc/kernel/postinst.d/zz-update-grub ] && rm /etc/kernel/postinst.d/zz-update-grub || true
	[ -f /etc/kernel/postrm.d/zz-update-grub ] && rm /etc/kernel/postrm.d/zz-update-grub || true
	[ -f /etc/initramfs/post-update.d/proxmox-boot-sync ] && rm /etc/initramfs/post-update.d/proxmox-boot-sync || true
}

###proxmox
##first deps
apt-get update && apt-get install -y wget lsb-release

##version pinning
[ "$(lsb_release -cs)" = "buster" ] && prx_rel=v6 || prx_rel=v7
echo "[PROXMOX $prx_rel chroot hook starting]"

##repo
echo "deb http://download.proxmox.com/debian/pve $(lsb_release -cs) pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list

##gpg key(s) download
#v6
[ "$prx_rel" = "v6" ] && wget http://download.proxmox.com/debian/proxmox-ve-release-6.x.gpg -O /etc/apt/trusted.gpg.d/proxmox-ve-release-6.x.gpg
#v7+
[ "$prx_rel" = "v7" ] && wget http://download.proxmox.com/debian/proxmox-release-$(lsb_release -cs).gpg -O /etc/apt/trusted.gpg.d/proxmox-release-$(lsb_release -cs).gpg

##pre-install deps
apt update && apt-get install -y postfix open-iscsi

##tricking ifupdown2 installation (prevent using ifreload)
echo "auto lo" >> /etc/network/interfaces
echo "iface lo inet loopback" >> /etc/network/interfaces
apt-get download ifupdown2
dpkg --unpack --force-conflicts ifupdown2*.deb
#rm /tmp/.ifupdown2-first-install
sed -i 's/ifreload -a/#ifreload -a/' /var/lib/dpkg/info/ifupdown2.postinst
dpkg --configure ifupdown2
apt-get install -yf
rm ifupdown2*.deb

##proxmox-ve
#all versions
echo "[INSTALLING PROXMOX $prx_rel]"
echo "127.0.0.1	localhost" > /etc/hosts
#v6
if [ "$prx_rel" = "v6" ]; then
	#apt-get install -y proxmox-ve 2>&1 | tee /tmp/proxmox-chroot-hook.log
	#below is dirty but in case of errors, we skip and continue
	apt-get install -y proxmox-ve >/tmp/proxmox-chroot-hook.log 2>&1 || true
	echo "[DISABLING ENTERPRISE REPO]"
	disable_repo
	echo "[BOOT HOOKS CLEANING]"
	boot_clean
	#stop the log flood by the PVE replication runner
	sed -i 's/^OnCalendar.*/OnCalendar=monthly/g' /usr/lib/systemd/system/pvesr.timer
else
#v7
	#apt-get download proxmox-ve
	#dpkg --unpack proxmox-ve*.deb
	
	#dpkg --configure proxmox-ve
	
	#echo "[BOOT CLEANING]"
	#boot_clean
	
	#dpkg --configure proxmox-ve
	
	#disable_repo
	#apt-get install -yf
	#rm proxmox-ve*.deb

	#apt-get install -y proxmox-ve 2>&1 | tee /tmp/proxmox-chroot-hook.log
	#below is dirty but in case of errors, we skip and continue
	apt-get install -y proxmox-ve >/tmp/proxmox-chroot-hook.log 2>&1 || true
	echo "[DISABLING ENTERPRISE REPO]"
	disable_repo
	echo "[BOOT HOOKS CLEANING]"
	boot_clean
fi

#[ $? -ne 0 ] && exit 1

#remove No valid subscription popup
#PLEASE SUPPORT THE PRODUCT
sed -i -z "s/res === null || res === undefined || \!res || res\n\t\t\t.data.status.toLowerCase() \!== 'active'/false/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js

##additional packages
#all versions
apt-get install -y sshfs ethtool openvswitch-switch usbip aoetools btrfs-progs net-tools
#v6
#[ $prx_rel = "v6" ] && apt-get install -y somepackage
#v7+
#[ $prx_rel = "v7" ] && apt-get install -y somepackage

#ceph install (will increase the final image size !)
#echo "deb http://download.proxmox.com/debian/ceph-nautilus $(lsb_release -cs) main" > /etc/apt/sources.list.d/ceph.list
#apt-get update && apt-get install -y ceph-base ceph-mgr ceph-mds ceph-mon ceph-osd

##livepve init services
ln -s /opt/livepve-network.service /etc/systemd/system/ && systemctl enable livepve-network
ln -s /opt/livepve-user.service /etc/systemd/system/ && systemctl enable livepve-user
ln -s /opt/livepve-storage.service /etc/systemd/system/ && systemctl enable livepve-storage
ln -s /opt/livepve-guests.service /etc/systemd/system/ && systemctl enable livepve-guests

##clean
[ -f /etc/network/interfaces.new ] && rm /etc/network/interfaces.new || true
#apt remove -y linux-image*
[ ! "$(dpkg --list |grep '^rc' | cut -d ' ' -f 3)" = "" ] && dpkg --list | grep "^rc" | cut -d " " -f 3 | xargs dpkg --purge
