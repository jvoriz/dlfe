#!/bin/bash

#Reading kernel command line
KRN_CMDLINE=$(cat /proc/cmdline)

#enable debian live user on pve pam realm
pve-liveuser ()
{
  pveuser="$(getent passwd 1000 | cut -d: -f1)"
  pveum useradd ${pveuser}@pam
  pveum aclmod / -user ${pveuser}@pam -roles Administrator
}

#create and enable bridge
pve-bridge ()
{
  for _PARAMETER in $KRN_CMDLINE
    do
      case "${_PARAMETER}" in
        live-config.pvebr=*|pvebr=*)
          #values order => name:type
          #pvebr="${_PARAMETER#*pvebr=}"
          pvebrarg="${_PARAMETER#*pvebr=}"
          pvebrvals=(${pvebrarg//:/ })
          pvebrname=${pvebrvals[0]}
          pvebrcfg=${pvebrvals[1]}
          #cleaning
          sed -i '/allow-hotplug/d' /etc/network/interfaces
          sed -i '/source-directory/d' /etc/network/interfaces
          #get current master interface to add the bridge to
          pvebrif="$(ip a | grep "state UP" | cut -d" " -f2 | cut -d":" -f1)"
          #set config type for bridge, dhcp or static (from pxe)
          case "$pvebrcfg" in
            frompxe)
              #import saved pxeboot network config
              . /tmp/netboot.save
cat <<EOT >> /etc/network/interfaces
auto ${pvebrname}
iface ${pvebrname} inet static
  address ${pxecidr}
  gateway ${pxegw}
  bridge-ports ${pvebrif}
  bridge-stp off
  bridge-fd 0
  bridge-vlan-aware yes
  bridge-vids 2-4094
EOT
            ;;
            static)
              #get additional static infos
              pvebrip=${pvebrvals[2]}
              pvebrmask=${pvebrvals[3]}
              pvebrgw=${pvebrvals[4]}
cat <<EOT >> /etc/network/interfaces
auto ${pvebrname}
iface ${pvebrname} inet static
  address ${pvebrip}/${pvebrmask}
  gateway ${pvebrgw}
  bridge-ports ${pvebrif}
  bridge-stp off
  bridge-fd 0
  bridge-vlan-aware yes
  bridge-vids 2-4094
EOT
            ;;
            dhcp|*)
cat <<EOT >> /etc/network/interfaces
auto ${pvebrname}
iface ${pvebrname} inet dhcp
	bridge-ports ${pvebrif}
	bridge-stp off
	bridge-fd 0
	bridge-vlan-aware yes
	bridge-vids 2-4094
EOT
            ;;
          esac
          #cleaning
          rm /tmp/netboot.save
          #set pxe boot interface to manual and restart it
          echo "restarting $pvebrif network interface"
          ifdown $pvebrif
          sed -i "s/^iface ${pvebrif} inet dhcp/iface ${pvebrif} inet manual/" /etc/network/interfaces
          ifup $pvebrif
          #get bridge up
          echo "bringing up $pvebrname bridge interface"
          ifup ${pvebrname}
        ;;
      esac
    done
}

#update local storage to remove rootdir content
pve-modlocalstorage ()
{
  pvesm set local --content snippets,images,iso,vztmpl,backup
}

#ramdisk for containers Volumes
pve-ctrd ()
{
  for _PARAMETER in $KRN_CMDLINE
    do
      case "${_PARAMETER}" in
        live-config.pvectrd=*|pvectrd=*)
          #values order => name:type:size
          ctrdarg="${_PARAMETER#*pvectrd=}"
          ctrdvals=(${ctrdarg//:/ })
          ctstorename=${ctrdvals[0]}
          ctstoretype=${ctrdvals[1]}
          ctstoresize=${ctrdvals[2]}

          #create dev node
          modprobe brd rd_size=$(($ctstoresize*1024))
          #create mountpoint
          mkdir /mnt/$ctstorename

          case "$ctstoretype" in
            ext4)
              #format
              mkfs.ext4 -L $ctstorename -O ^has_journal -E lazy_itable_init=0 -m 0 /dev/ram0
              #tune
              tune2fs -i 0 -c 0 /dev/ram0
              #mount
              mount -o noatime /dev/ram0 /mnt/$ctstorename
            ;;
            btrfs)
              #format
              mkfs.btrfs -L $ctstorename -K -d single /dev/ram0
              #mount
              mount -o nodatacow /dev/ram0 /mnt/$ctstorename
            ;;
          esac

          #declare pve storage if mount command success
          [ $? -eq 0 ] && pvesm add dir $ctstorename --path /mnt/$ctstorename --content rootdir
        ;;
      esac
    done
}

#additional storages
pve-store ()
{
  for _PARAMETER in $KRN_CMDLINE
    do
      case "${_PARAMETER}" in
        live-config.pvestore=*|pvestore=*)
          #generic values order
          #local context => name:context:type:device:mount_opts:content:pvesm_opts
          #remote context => name:context:type:user:password:remotehost:port:remotepath:content:pvesm opts
          pvestorearg="${_PARAMETER#*pvestore=}"
          #pvestorevals=(${pvestorearg//:/ })
          IFS=":" read -ra pvestorevals <<< "$pvestorearg"
          pvestorename=${pvestorevals[0]}
          pvestorecontext=${pvestorevals[1]}

          #create mountpoint
          mkdir /mnt/$pvestorename

          case "$pvestorecontext" in
            local)
              pvestoretype=${pvestorevals[2]}
              pvestoredev=${pvestorevals[3]}
              pvemountopts=${pvestorevals[4]}
              pvestorecont=${pvestorevals[5]}
              pvestoreopts=${pvestorevals[6]}
              case "$pvestoretype" in
                zfs)
                  #set pvesm storage type
                  pvesm_storetype="zfspool"
                  #import
                  zpool import -f -o $pvemountopts $pvestoredev
                ;;
                lvm)
                  #set pvesm storage type
                  pvesm_storetype="lvm"
                ;;
                lvmthin)
                  #set pvesm storage type
                  pvesm_storetype="lvmthin"
                ;;
                *)
                  #set pvesm storage type
                  pvesm_storetype="dir"
                  #standard device mount
                  mount -t $pvestoretype -o $pvemountopts $pvestoredev /mnt/$pvestorename
                ;;
              esac
            ;;
            remote)
              pvestoretype=${pvestorevals[2]}
              pvestoreuser=${pvestorevals[3]}
              pvestorepass=${pvestorevals[4]}
              pvestorehost=${pvestorevals[5]}
              pvestoreport=${pvestorevals[6]}
              pvestorepath=${pvestorevals[7]}
              pvestorecont=${pvestorevals[8]}
              pvestoreopts=${pvestorevals[9]}
              case "$pvestoretype" in
                ssh)
                  #set pvesm storage type
                  pvesm_storetype="dir"
                  #ssh options
                  ssh_opts="-o cache=yes -o compression=no -o StrictHostKeyChecking=no"
                  #fuse options
                  fuse_opts="-o kernel_cache -o password_stdin"
                  #mount
                  echo $pvestorepass | sshfs $ssh_opts -p $pvestoreport "$pvestoreuser@$pvestorehost:$pvestorepath" /mnt/$pvestorename $fuse_opts
                ;;
                nfs)
                  #set pvesm storage type
                  pvesm_storetype="nfs"
                ;;
                cifs)
                  #set pvesm storage type
                  pvesm_storetype="cifs"
                ;;
              esac
            ;;
          esac

          #declare pve storage if mount command success
          if [ $? -eq 0 ]; then
            case "$pvesm_storetype" in
              dir)
                pvesm add $pvesm_storetype $pvestorename --path /mnt/$pvestorename --content $pvestorecont $pvestoreopts
              ;;
              zfspool)
                pvesm add $pvesm_storetype $pvestorename --pool $pvestoredev --content $pvestorecont $pvestoreopts
              ;;
              lvm)
                pvesm add $pvesm_storetype $pvestorename --vgname $pvestoredev --content $pvestorecont $pvestoreopts
              ;;
              lvmthin)
                pvesm add $pvesm_storetype $pvestorename --vgname $pvestoredev --thinpool $pvemountopts --content $pvestorecont $pvestoreopts
              ;;
              nfs)
                pvesm add $pvesm_storetype $pvestorename --path /mnt/$pvestorename --server $pvestorehost --export $pvestorepath --content $pvestorecont $pvestoreopts
              ;;
              cifs)
                pvesm add $pvesm_storetype $pvestorename --path /mnt/$pvestorename --server $pvestorehost --share $pvestorepath --username $pvestoreuser --password $pvestorepass --content $pvestorecont $pvestoreopts
              ;;
            esac
            [ $? -ne 0 ] && echo "error adding storage $pvestorename"
          else
            echo "error initializing storage $pvestorename"
          fi
        ;;
      esac
    done
}

#guests to restore after system startup
pve-guests ()
{
  for _PARAMETER in $KRN_CMDLINE
    do
      case "${_PARAMETER}" in
        live-config.pveguest=*|pveguest=*)
          #values order => id:type:archive storage name:restore storage name:autostart:opts
          pveguestarg="${_PARAMETER#*pveguest=}"
          pveguestvals=(${pveguestarg//:/ })
          pveguestid=${pveguestvals[0]}
          pveguesttype=${pveguestvals[1]}
          pveguestsrcstorage=${pveguestvals[2]}
          pveguestdststorage=${pveguestvals[3]}
          pveguestautostart=${pveguestvals[4]}
          pveguestopts=${pveguestvals[5]}

          ##check and wait for mountpoints to be ready
          #src storage
          while true; do mountpoint -q "/mnt/$pveguestsrcstorage" && break || sleep 2; done
          #dst storage (but local)
          [ ! $pveguestdststorage = "local" ] && while true; do mountpoint -q "/mnt/$pveguestdststorage" && break || sleep 2; done

          echo starting restoring vm $pveguestid from $pveguestbackuparchive
          case "$pveguesttype" in
            vm)
              #get last backup archive
              pveguestbackuparchive="/mnt/$pveguestsrcstorage/dump/$(ls /mnt/$pveguestsrcstorage/dump | grep "qemu-$pveguestid" | grep -v log | tail -1)"
              #restore
              qmrestore $pveguestbackuparchive $pveguestid --storage $pveguestdststorage $pveguestopts
              #autostart
              [ $pveguestautostart -eq 1 ] && qm start $pveguestid
            ;;
            ct)
              #get last backup archive
              pveguestbackuparchive="/mnt/$pveguestsrcstorage/dump/$(ls /mnt/$pveguestsrcstorage/dump | grep "lxc-$pveguestid" | grep -v log | tail -1)"
              #restore
              pct restore $pveguestid $pveguestbackuparchive --storage $pveguestdststorage --start $pveguestautostart $pveguestopts
            ;;
          esac

        ;;
      esac
    done
}

#Main
case "${1:-}" in
  start)
    echo starting script $0 with args $1 $2
    case "${2:-}" in
      user)
        pve-liveuser
      ;;
      network)
        pve-bridge
      ;;
      storage)
        pve-modlocalstorage
        pve-ctrd
        pve-store
      ;;
      guests)
        pve-guests
      ;;
    esac
  ;;
  stop)
    echo stopping script $0 with args $1 $2
  ;;
  status)
    echo status of script $0
  ;;
  *)
    echo "Usage: ${0:-} {start|stop|status}" >&2
    exit 1
  ;;
esac
