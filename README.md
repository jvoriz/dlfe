# DLFE (Debian Live For Everything)
Debian live-build profiles with additional scripts, hooks... to build and configure Debian based live systems.

# Credits
- Debian & Debian Live Team
- Live-build scripts authors
- Proxmox
- Docker

# History
I finally build and use live systems because:

- It does not make sense to keep servers running for doing testing, rendering or any other time-limited task
- I'm lazy trying to find controlers / disks for system booting, that are well accepted by mainstream servers (without fan going noisy etc)
- I already have an always on and low power consumption ARM host on my LAN to store and serve stuff

# Requirement
- DHCP/TFTP server
- Plenty of RAM on the node(s) to live boot

# Building on Debian
* clone this repo
* optionnal: define Debian mirrors in global/mirrors.conf (see example in dist directory)
* optionnal: define proxies to speed up building in global/proxy.conf (see example in dist directory)
* run build.sh script as root

## Interactive build
* run script without args `./build.sh`

## Specify target options
* use args: <profile_dir> <debian_release> <target_architecture> <target_image>

`./build.sh chromium bullseye amd64 tar`

live-build supported target_image: > iso iso-hybrid netboot tar hdd

tested target images: > iso-hybrid tar

## Batch (re)build
To update Debian packages inside your livesystems, it is possible to batch rebuild all previously _and_ successfully builded profiles with special arg REBUILD_ALL `./build.sh REBUILD_ALL`

## ARM / foreign architecture
**qemu-user-static** package will be installed before trying to build a live system for a foreign architecture, 
if you build your own custom systems on a different target architecture, remind to add two more lines in the profile's auto/config file:

example for an arm64 target, builded on an amd64 host:
```
--bootstrap-qemu-arch arm64
--bootstrap-qemu-static /usr/bin/qemu-aarch64/static
```

## configure
Global configuration: use/put .conf files in global dir (copy from dist dir) to fit your needs

Some config files are available globaly (applied for all profiles), and also localy (if present) in each profile directory to add/override config options, like prebuild.conf and postbuild.conf

### build.conf
Global options only

### prebuild.conf
Define here options used before the profile's build process is starting
- chroot packages to be installed
- common hooks and scripts (some are modified from original), copied from common dir

### postbuild.conf
Define here options used after the profile's build process is successfull
- tftp server settings
- pxe menu options (generate example pxelinux menu and optionnaly refresh existing menu on remote host)
- scp / rsync option to push new builded files

# NETWORK BOOT
After building finished, put the following generated files from profiles/<profile_name>/binary/live/ to your dhcp/tftp server and use them to boot your node(s):
- vmlinuz
- initrd.img
- filesystem.squashfs

## Proxmox example
```
LABEL proxmox testing (6.3.4 / 5.4.98-1)
 KERNEL boot/live/proxmox/vmlinuz
 APPEND initrd=boot/live/proxmox/initrd.img pvebr=vmbr51:frompxe pvestore=tank:remote:ssh:root:pass:tank:222:/srv/dev-disk-by-label-tools/pvestore:snippets,images,iso,vztmpl,backup pvectrd=ramctvols:btrfs:1024 pveguest=100:ct:tank:ramctvols:1 pveguest=101:vm:tank:local:1 ethdevice-timeout=30 loglevel=4 hostname=macaddr nottyautologin boot=live timezone=Europe/Paris keyboard-layouts=fr username=pveadm fetch=http://tank/tftp/boot/live/proxmox/filesystem.squashfs
```

## Docker example
```
LABEL docker amd64
 KERNEL boot/live/docker/amd64/vmlinuz
 APPEND initrd=boot/live/docker/amd64/initrd.img loglevel=5 portainer=1 hostname=macaddr noeject nottyautologin boot=live timezone=Europe/Paris keyboard-layouts=fr username=dockadm user-default-groups=audio,cdrom,dip,floppy,video,plugdev,netdev,docker fetch=http://tank/tftp/boot/live/docker/amd64/filesystem.squashfs
```

## ARM
> remind to add devicetreedir boot option for your ARM device, that is "BIOS less"
```
label docker arm64
 kernel boot/live/docker/arm64/vmlinuz-4.19.0-14-arm64
 initrd boot/live/docker/arm64/initrd.img-4.19.0-14-arm64
 devicetreedir boot/arm64/dtbs
 append loglevel=5 portainer=1 hostname=macaddr noeject nottyautologin boot=live timezone=Europe/Paris keyboard-layouts=fr username=itadmin user-default-groups=audio,cdrom,dip,floppy,video,plugdev,netdev,docker ethdevice-timeout=30 fetch=http://tank/tftp/boot/live/docker/arm64/filesystem.squashfs
```


# BOOT ARGS

## live-boot general
- **ethdevice-timeout=30** increase default timeout (15) for ethernet device, usefull when timeout occurs on some ethernet interfaces before trying to download the squashfs
- **overlay-size=80%** increase available space in RAM for the tmpfs mount (or "local" storage for proxmox), usefull if you need to store big files / vm disks in RAM, but be carefull to leave enough RAM available for the system

## live-config general
- **nottyautologin** prevent autologin on local console after booting

## dlfe commons
- **hostname=macaddr** generate hostname based on the current active network interface hardware address

## docker
### portainer=
auto start the embed portainer image after booting

syntax: portainer=(0|1)

autostart: `portainer=1`

## proxmox
### pvebr=
set name and add a bridge on the first ethernet interface that has an UP state (usually the one used to pxe boot)

syntax: pvebr=**bridge_name**:**bridge_config**(dhcp|frompxe|static:cidr:gw)

dhcp: `pvebr=vmbr0:dhcp`

static: `pvebr=vmbr0:static:192.168.23.2/24:192.168.23.1`

> to quickly setup a cluster choose either "frompxe" or "static" option


### pvectrd=
set name, filesystem and size(M) of a formated ramdisk to be used for containers because of the O_DIRECT tmpfs/ramfs limitation

syntax: pvectrd=**storage_name**:**filesystem**(ext4|btrfs):**size**

1Gb btrfs: `pvectrd=ctramdisk:btrfs:1024`


### pvestore=
set additional storage

remote storage syntax: pvestore=**storage_name**:_remote_:**type**(ssh|smb|nfs):**user**:**password**:**remotehost**:**port**:**remotepath**:**content-type**:**other-pvesm-options**

local storage syntax: pvestore=**storage_name**:_local_:**device**:**content-type**:**other-pvesm-options**

sshfs: `pvestore=remstore:remote:ssh:root:pass:srv:222:/tank/pvestore:snippets,images,iso,vztmpl,backup`

/dev/sda1: `pvestore=usbstore:local:/dev/sda1:snippets,images,iso,vztmpl,backup`


> available content-type (Proxmox 6.x): iso,images,rootdir,backup,vztmpl,snippets

> "local" storage name is not allowed


### pveguest=
auto restore after system started and optionally auto start guest(s) (containers and/or vms)

syntax: pveguest=**guest_id**:**guest_type**:**storage_name_to_restore_from**:**storage_name_to_restore_to**:**autostart**

restore and start ct id 100 from tank storage to ramctvols storage: `pveguest=100:ct:tank:ramctvols:1`

## Chromium
### Kiosk
to enable kiosk mode: `kiosk=1`
### Startpage
to provide browser startpage (not 'homepage'): `startpage=https://www.mywebsite.domain`


# TODO
- [ ] Proxmox: Test CIFS pvestore remote storage mounting
- [ ] Proxmox: Test NFS pvestore remote storage mounting
- [ ] General: Add capability to define user password 


# Limitations
## Proxmox
- tmpfs/ramfs file systems don't support O_DIRECT => avoid "no cache" and "directsync" for vm disks cache option


# Known issues
- use same debian release for builder host as target (or newer), to avoid some issues
- at shudown there is an error about unmounting /run/live/medium, it seems that network is un-configured before unmounting
