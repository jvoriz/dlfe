#!/bin/bash

#########################################################################################################################
## first thing first
#########################################################################################################################

set -a
#get this script absolute path
SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
#src
common_dir=$SCRIPT_DIR/common
global_dir=$SCRIPT_DIR/global
tools_dir=$SCRIPT_DIR/tools
dist_dir=$SCRIPT_DIR/dist
#lb
hooks_dir=config/hooks
chroot_includes_dir=config/includes.chroot

##sourcing basic stuff
#prettify messages
[ -f $global_dir/prettify.conf ] && . $global_dir/prettify.conf
#functions
[ -f $tools_dir/functions.sh ] && . $tools_dir/functions.sh
#build config
dist_file_to_global "build.conf"
file_sourcing $global_dir "build.conf"
#check/create builds dir
[ ! -d $SCRIPT_DIR/$builds_dir ] && mkdir -p $SCRIPT_DIR/$builds_dir
set +a

##check root user or guit
if [ "$(id -u)" -ne 0 ]; then
  fail_msg "Please run as root" quit
fi
#########################################################################################################################

#########################################################################################################################
## check/upgrade required build packages
#########################################################################################################################
packages_check_upgrade "$reguired_building_packages"
#########################################################################################################################

#########################################################################################################################
## interactive profile selection (if no args)
#########################################################################################################################
if [ $# -eq 0 ]; then
	#ask for build profile
	clear
	shopt -s dotglob
	shopt -s nullglob
	profiles=($SCRIPT_DIR/profiles/*)
	PS3='Choose a build profile, or ctrl+c to abort: '
	select profile in "${profiles[@]##*/}";
	do
		export profile=$profile
	break;
	done
	#ask for release
	clear
	PS3='Choose a release, or ctrl+c to abort: '
	select release in ${releases};
	do
  		export release=$release
  	break;
	done
	#ask for architecture
	clear
	PS3='Choose an architecture (THAT IS VIABLE FOR YOUR TARGET !), or ctrl+c to abort: '
	select arch in ${archs};
	do
  		export arch=$arch
  	break;
	done
	#ask for binary image
	clear
	PS3='Choose the target binary image (iso|iso-hybrid|netboot|tar|hdd), or ctrl+c to abort: '
	select binimg in ${binimgs};
	do
  		export binimg=$binimg
  	break;
	done
else
	if [ $# -eq 4 ]; then
		export profile=$1
		export release=$2
		export arch=$3
		export binimg=$4
	else
		if [ $# -eq 1 ] && [ "$1" = "REBUILD_ALL" ]; then
			profile="$1"
		else
			fail_msg "Bad args, script usage: $0 <profile_dir> <debian_release> <target_architecture> <target_image>" quit
		fi
	fi
fi
#########################################################################################################################

#########################################################################################################################
## (re)build all EXISTING AND SUCCESSFULL builds (if asked with special profile name REBUILD_ALL)
if [ "$profile" = "REBUILD_ALL" ]; then
	pushd $builds_dir >/dev/null
	builded_profiles="$(find * -maxdepth 0 -type d 2>/dev/null)"
	if [ -z "$builded_profiles" ]; then
		fail_msg "[No builded profiles found in $builds_dir, exiting]"
		popd >/dev/null
		exit 1
	else
		for builded_profile in $builded_profiles ; do
			pushd $builded_profile >/dev/null
			builded_releases="$(find * -maxdepth 0 -type d)"
			for builded_release in $builded_releases ; do
				pushd $builded_release >/dev/null
				builded_archs="$(find * -maxdepth 0 -type d)"
					for builded_arch in $builded_archs ; do
						pushd $builded_arch >/dev/null
						#builded_imgs="$(find * -maxdepth 0 -type d)"
						[ -f build.success ] && builded_imgs="$(cat build.success)" || warn_msg "[No successfull builds found for $builded_profile $builded_release $builded_arch, skipping rebuilding]"
						for builded_img in $builded_imgs ; do	
							echo -n "Rebuilding $builded_profile $builded_release $builded_arch $builded_img "
							$SCRIPT_DIR/$(basename $0) $builded_profile $builded_release $builded_arch $builded_img 2>&1 >/dev/null
							[ "$?" -eq 0 ] && good_msg || fail_msg
						done
						builded_imgs=""
						popd >/dev/null
					done
				popd >/dev/null
			done
    		popd >/dev/null
		done
		popd >/dev/null
		exit 0
	fi
fi
#########################################################################################################################

#########################################################################################################################
info_msg "[Starting build of profile ($profile $release $arch => $binimg)]"
#########################################################################################################################

#########################################################################################################################
## set profile_dir build_dir binaries_dir logs_dir
#########################################################################################################################
set -a
profile_dir=$SCRIPT_DIR/profiles/$profile
build_dir=$SCRIPT_DIR/$builds_dir/$profile/$release/$arch
binaries_dir=$build_dir/binary/live
logs_dir=$build_dir/logs
set +a
#########################################################################################################################

#########################################################################################################################
## BUILD DIR INIT
#########################################################################################################################
info_msg "[Initializing build directory ($profile $release $arch => $binimg)]"
#check/create this target build dir
[ ! -d $build_dir ] && mkdir -p $build_dir
#check/create this target logs dir
[ ! -d $logs_dir ] && mkdir -p $logs_dir
#add live-build auto dir and config script if not present, from common one
[ ! -d $build_dir/auto ] && mkdir -p $build_dir/auto
[ ! -x $build_dir/auto/config ] && cp -p $SCRIPT_DIR/common/auto/config $build_dir/auto/config
#########################################################################################################################

#########################################################################################################################
## PROFILE DIR INIT
#########################################################################################################################
info_msg "[Initializing profile directory ($profile $release $arch => $binimg)]"
#check/create profile dir
if [ ! -d $profile_dir ]; then
	warn_msg "profile directory $profile_dir is not found, creating/building new empty one"
	mkdir -p $profile_dir
fi
#entering profile dir
pushd $profile_dir >/dev/null
##Profile config files
conf_files="prebuild.conf postbuild.conf"
dist_file_to_global "$conf_files" && global_file_to_local $profile_dir "$conf_files"
#exiting profile dir
popd >/dev/null
#########################################################################################################################

#########################################################################################################################
## PROXY/MIRRORS
#########################################################################################################################
#source global proxies / mirrors if defined (see dist files for example)
file_sourcing $global_dir "proxy.conf mirrors.conf"
#########################################################################################################################

#########################################################################################################################
## PREBUILD
#########################################################################################################################
#start prebuild script if present
if [ ! -x $SCRIPT_DIR/tools/prebuild.sh ]; then
	fail_msg "[prebuild script not found or not executable]" quit
else
	info_msg "[found prebuild script, executing...]"
	$SCRIPT_DIR/tools/prebuild.sh 2>&1 | tee $logs_dir/prebuild.log
	if [ ! "${PIPESTATUS[0]}" -eq "0" ]; then
		fail_msg "[error executing prebuild script]"
		info_msg "=> check logs in $logs_dir/prebuild.log"
		exit 1
	else
		good_msg "[success executing prebuild script]"
	fi
fi
#########################################################################################################################

#########################################################################################################################
## BUILD
#########################################################################################################################
#go inside build dir
pushd $build_dir >/dev/null
#clean
lb clean
#build
lb_prx=$lb_prx lb_mir=$lb_mir target_rel=$release target_arch=$arch target_img=$binimg lb build 2>&1 | tee $logs_dir/build.log
if [ ! "${PIPESTATUS[0]}" -eq "0" ]; then
	fail_msg "[error building live $profile ($release $arch)]"
	info_msg "=> check logs in $logs_dir/build.log"
	#exiting build dir
	popd >/dev/null
	#exit now
	exit 1
else
	good_msg "[successfully builded live $profile ($release $arch)]"
	info_msg "=> live binaries are in $build_dir/binary/live"
	#exiting build dir
	popd >/dev/null
fi
#########################################################################################################################

#########################################################################################################################
## POSTBUILD
#########################################################################################################################
if [ ! -x $SCRIPT_DIR/tools/postbuild.sh ]; then
	fail_msg "[postbuild script not found or not executable]" quit
else
	info_msg "[found postbuild script, executing...]"
	$SCRIPT_DIR/tools/postbuild.sh 2>&1 | tee $logs_dir/postbuild.log
	if [ ! "${PIPESTATUS[0]}" -eq "0" ]; then
		fail_msg "[error executing postbuild script]"
		info_msg "=> check logs in $logs_dir/postbuild.log"
		exit 1
	else
		good_msg "[success executing postbuild script]"
	fi
fi
#########################################################################################################################

#########################################################################################################################
## SUCCESSFULL BUILD RECORDING
#########################################################################################################################
#add this binary image as successfull builded (to be used with REBUILD_ALL)
file_append_line $build_dir/build.success "$binimg"
#########################################################################################################################
